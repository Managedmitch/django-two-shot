from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def receipt_view(request):
    receipt_view = Receipt.objects.filter(purchaser=request.user)
    context = {"views": receipt_view}
    return render(request, "receipts/receipt.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
        return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "receipt_form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def catrgory_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories": category}
    return render(request, "categories/categories_list.html", context)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {"accounts": account}
    return render(request, "accounts/accounts_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
        return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "category_form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
        return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "account_form": form,
    }
    return render(request, "receipts/create_account.html", context)
